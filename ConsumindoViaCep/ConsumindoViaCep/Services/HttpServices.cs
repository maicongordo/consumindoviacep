﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsumindoViaCep.Services
{
    class HttpServices
    {

        static readonly HttpClient Client = new HttpClient();

        public static async Task<HttpResponseMessage>

            GetRequest(string url)
        {
            HttpResponseMessage response = await Client.GetAsync(url);

            return response;
        }

        public static async Task<HttpResponseMessage>
            PostRequest(string url, object obj)
        {

            string json = JsonConvert.SerializeObject(obj);

            StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await Client.PostAsync(url, stringContent);

            return response;
        }

        public static JObject
            GetErrorDataFromHttpResponseMessege(HttpResponseMessage response)

        {
            var data = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            return data;

        }

    }
}
