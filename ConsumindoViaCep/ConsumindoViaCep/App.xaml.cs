﻿using ConsumindoViaCep.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ConsumindoViaCep
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new EnderecoCep();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
